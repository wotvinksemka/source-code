import java.util.Scanner;

public class LinkMaker {
	public static String steamSearchLink = "https://store.steampowered.com/search/?term=";
	public String name = null;

	public LinkMaker() {
		super();
	}

	public String makeLink(String nameOfTheGame) {
		this.name = nameOfTheGame;
		return steamSearchLink + this.name.replaceAll("\\s+", "+");

	}

}
